from mysqlapp.app import app
from mysqlapp.app.controller import reservasi_controller
from flask import Blueprint, request

reservasi_blueprint = Blueprint("reservasi_router", __name__)

@app.route("/reservasi", methods=["GET"])
def showReservasi():
    return reservasi_controller.shows()

@app.route("/reservasi/insert", methods=["POST"])
def insertReservasi():
    params = request.json
    return reservasi_controller.insert(**params)

@app.route("/reservasi/status", methods=["POST"])
def updateStatus():
    return reservasi_controller.changeStatus()

