from mysqlapp.app import app
from mysqlapp.app.controller import tamu_controller
from flask import Blueprint, request

tamu_blueprint = Blueprint("tamu_router", __name__)

@app.route("/tamu", methods=["GET"])
def showUsers():
    return tamu_controller.shows()

@app.route("/tamu", methods=["GET"])
def showUser():
    params = request.json
    return tamu_controller.show(**params)

@app.route("/tamu/insert", methods=["POST"])
def insertUser():
    params = request.json
    return tamu_controller.insert(**params)

@app.route("/tamu/update", methods=["POST"])
def updateUser():
    params = request.json
    return tamu_controller.update(**params)

@app.route("/tamu/delete", methods=["POST"])
def deleteUser():
    params = request.json
    return tamu_controller.delete(**params)

@app.route("/tamu/requesttoken", methods=["GET"])
def requesttoken():
    params = request.json
    return tamu_controller.token(**params)