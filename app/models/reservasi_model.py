from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='hotel',
                              user='root',
                              password='password'
            )
        except Exception as e:
            print(e)

    def showReservasiByEmail(self, **params):
        cursor = self.db.cursor()
        query = '''
        select tamu.nama_tamu,reservasi.*
        from reservasi
        inner join tamu on reservasi.tamuid = tamu.tamuid
        where tamu.email = "{0}" and reservasi.is_active = 1;
        '''.format(params["email"])
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def insertReservasi(self, **params):
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))
        cursor = self.db.cursor()
        query = '''
        insert into ({0})
        values {1};
        '''.format(column, values)
        cursor.execute(query)

    def updateReservasi(self, **params):
        reservasiid = params["reservasiid"]
        cursor = self.db.cursor()
        query ='''
        UPDATE reservasi
        SET is_active = 0
        WHERE reservasiid = {1};
        '''.format(reservasiid)
        cursor.execute(query)

    def dataCommit(self):
        self.db.commit()







#
# class reservasi(Base):
#     __tablename__ = 'reservasi'
#     reservasiid = Column(Integer, primary_key=True)
#     nama_kamar = Column(String)
#     tgl_masuk = Column(Date)
#     tgl_keluar = Column(Date)
#     is_active = Column(Integer)
#     # tamuid = Column(Integer, ForeignKey('tamuid'))

