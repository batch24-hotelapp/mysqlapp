from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import exc, delete, update


Base = declarative_base()


class database:
    # Isi parameter dari database mysql
    def __init__(self):
        engine = create_engine('mysql+mysqlconnector://root:password@localhost:3306/hotel', echo=True)
        Session = sessionmaker(bind=engine)
        self.session = Session()

        if self.session:
            print("Connection Success")

    def row2dict(self, row):
        self.d = {}
        for column in row.__table__.columns:
            self.d[column.name] = str(getattr(row, column.name))
        return self.d

    def showUsers(self):
        resultdb = self.session.query(tamu).all()
        result = [self.row2dict(row) for row in resultdb]
        return result

    def showUsersById(self, **params):
        resultdb = self.session.query(tamu).filter(tamu.tamuid == params['tamuid']).one()
        result = self.row2dict(resultdb)
        return result

    def showUserByEmail(self, **params):
        resultdb = self.session.query(tamu).filter(tamu.email == params['email']).one()
        result = self.row2dict(resultdb)
        return result

    def insertUser(self, **params):
        try:
            self.session.add(tamu(**params))
            return self.session.commit()
        except exc.IntegrityError as e:
            self.session.rollback()
            print("Data sudah ada dalam basis data: {}".format(e))

    def updateUserById(self, **params):
        query_result = self.session.query(tamu).filter(tamu.tamuid == params["tamuid"])
        query_result.update(params)
        self.session.commit()

    def deleteUserById(self, **params):
        query_result = self.session.query(tamu).filter(tamu.tamuid == params["tamuid"])
        query_result.delete()
        self.session.commit()

    def dataCommit(self):
        self.session.commit()

    def closeConnection(self):
        if self.session is not None and self.session.is_connected():
            self.session.close()



class tamu(Base):
    __tablename__ = 'tamu'
    tamuid = Column(Integer, primary_key=True)
    nama_tamu = Column(String)
    alamat = Column(String)
    no_telp = Column(String)
    email = Column(String)





