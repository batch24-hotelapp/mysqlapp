from mysqlapp.app.models.tamu_model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()

def shows():
    dbresult = mysqldb.showUsers()
    return jsonify(dbresult)

def show(**params):
    dbresult = mysqldb.showUsersById(**params)
    return jsonify(dbresult)

def insert(**params):
    mysqldb.insertUser(**params)
    mysqldb.dataCommit()
    return jsonify({"message" : "Success"})

def update(**params):
    mysqldb.updateUserById(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def delete(**params):
    mysqldb.deleteUserById(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def token(**params):
    dbresult = mysqldb.showUserByEmail(**params)
    if dbresult is not None:
        # Payload untuk JWT

        tamu = dbresult

        expires = datetime.timedelta(days=1)
        access_token = create_access_token(tamu, fresh=True, expires_delta=expires)

        data = {
            "data" : tamu,
            "token_access" : access_token
        }
    else:
        data = {
            "message" : "Email tidak terdaftar"
        }

    return jsonify(data)

