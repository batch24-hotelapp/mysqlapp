from mysqlapp.app.models.reservasi_model import database
from mysqlapp.app.models.tamu_model import database as tamu_db
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime, requests

mysqldb = database()
tamu_db = tamu_db()

@jwt_required()
def shows():
    # ambil payload dari token
    params = get_jwt_identity()
    dbresult = mysqldb.showReservasiByEmail(**params)
    result = []
    if dbresult is not None:
        for items in dbresult:
            id = json.dumps({"id":items[6]})
            kamardetail = getKamarById(id)
            user = {
                "nama_tamu": items[0],
                "reservasiid": items[1],
                "nama_kamar" : items[2],
                "tgl_masuk": items[3],
                "tgl_keluar": items[4],
                "kamarid": items[6],
                "tipe_kamar": kamardetail["tipe"],
                "price" : kamardetail["harga"]
            }
            result.append(user)
        else:
            result = dbresult

    return jsonify(result)


@jwt_required()
def insert(**params):
    token = get_jwt_identity()
    tamuid = tamu_db.showUserByEmail(**token)[0]
    tgl_masuk = datetime.datetime.now().isoformat()
    tgl_keluar = datetime.datetime.now().isoformat()
    id = json.dumps({"id":params["kamarid"]})
    nama_kamar = getKamarById(id)["nama"]
    params.update(
        {
            "tamuid": tamuid,
            "tgl_masuk": tgl_masuk,
            "tgl_keluar": tgl_keluar,
            "nama_kamar": nama_kamar,
            "is_active": 1
        }
    )
    mysqldb.insertReservasi(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

@jwt_required()
def changeStatus(**params):
    mysqldb.updateReservasi(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def getKamarById(data):
    kamar_data = requests.get(url="http://localhost:8000/kamarbyid", data=data)
    return kamar_data.json()