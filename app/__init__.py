from flask import Flask
from mysqlapp.config import Config
from flask_jwt_extended import JWTManager


app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

from mysqlapp.app.routers.tamu_router import *
from mysqlapp.app.routers.reservasi_router import *

app.register_blueprint(tamu_blueprint)
app.register_blueprint(reservasi_blueprint)